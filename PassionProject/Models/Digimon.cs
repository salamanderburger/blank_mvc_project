﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class Digimon
    {
        //digimon ID
        [Key]
        public int digimonID { get; set; }

        //digimon name

        //digimon powers

        //digimon's trainer
        public virtual Trainer trainer { get; set; }

    }
}