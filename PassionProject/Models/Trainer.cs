﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class Trainer
    {
        //trainerid
        [Key]
        public int trainerid { get; set; }
        //trainername

        //collection of digimon
        public virtual ICollection<Digimon> Digimon { get; set; }
    }
}